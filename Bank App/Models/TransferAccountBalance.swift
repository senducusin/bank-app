//
//  TransferAccountBalance.swift
//  Bank App
//
//  Created by Jansen Ducusin on 5/18/21.
//

import Foundation

struct TransferAccountBalance: Codable {
    let accountFromId: String
    let accountToId: String
    let amount: Double
}

extension TransferAccountBalance {
    static func transferBalance(transferRequest: TransferAccountBalance) -> Resource<Response>?{
        guard let data = try? JSONEncoder().encode(transferRequest),
              let url = URL(string: "http://jarvis-nodejs.herokuapp.com/api/bank/transfer") else {return nil}
        
        return Resource(url: url, httpMethod: .post, httpBody: data)
    }
}
