//
//  Response.swift
//  Bank App
//
//  Created by Jansen Ducusin on 5/18/21.
//

import Foundation

struct Response:Codable {
    var status: Bool
    var message: String?
}
