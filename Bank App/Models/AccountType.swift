//
//  AccountType.swift
//  Bank App
//
//  Created by Jansen Ducusin on 5/17/21.
//

import Foundation

enum AccountType: String, Codable, CaseIterable {
    case checking
    case saving
}

extension AccountType {
    var title: String {
        self.rawValue.capitalized
    }
}
