//
//  Account.swift
//  Bank App
//
//  Created by Jansen Ducusin on 5/17/21.
//

import Foundation

struct Account:Codable {
    var id: UUID?
    var name: String
    let accountType: AccountType
    var balance: Double
}

extension Account {
    static func all() -> Resource<[Account]>?{
        guard let url = URL(string: "http://jarvis-nodejs.herokuapp.com/api/bank/accounts") else {return nil}
        
        return Resource(url: url)
    }
    
    static func create(account: Account) -> Resource<Response>?{
        guard let data = try? JSONEncoder().encode(account),
              let url = URL(string: "http://jarvis-nodejs.herokuapp.com/api/bank/create") else {return nil}
        
        return Resource(url: url, httpMethod: .post, httpBody: data)
    }
    
}
