//
//  AccountSummary.swift
//  Bank App
//
//  Created by Jansen Ducusin on 5/18/21.
//

import SwiftUI

enum ActiveSheet: Identifiable {
    var id:UUID {
        UUID()
    }
    
    case none
    case addAccount
    case transferBalance
}

struct AccountSummary: View {
    
    @ObservedObject private var viewModel = AccountSummaryViewModel()
    
    @State private var activeSheet: ActiveSheet? = nil
    
    var body: some View {
        
        GeometryReader { geometry in
            VStack {
                AccountListView(accounts: viewModel.accounts)
                    .frame(height: geometry.size.height/1.5)
                
                Text(viewModel.total.formatAsCurrency())
                
                Spacer()
                
                Button("Transfer Balance"){
                    activeSheet = .transferBalance
                    
                }.padding()
            }
        }
        .onAppear{
            viewModel.getAllAccounts()
        }
        .sheet(item: $activeSheet,onDismiss:{
            viewModel.getAllAccounts()
        }, content: { item in
            if item == .addAccount {
                CreateAccount()
            }else if item == .transferBalance {
                TransferBalance()
            }
        })
        .navigationBarItems(trailing: Button("Add Account"){
            activeSheet = .addAccount
            
        })
        .navigationBarTitle("Account Summary")
        .embedInNavigationView()
    }
}

struct AccountSummary_Previews: PreviewProvider {
    static var previews: some View {
        AccountSummary()
    }
}
