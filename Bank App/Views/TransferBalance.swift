//
//  TransferBalance.swift
//  Bank App
//
//  Created by Jansen Ducusin on 5/18/21.
//

import SwiftUI

struct TransferBalance: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject private var viewModel = TransferBalanceViewModel()
    @State private var showSheet = false
    @State private var isFromAccount = false
    
    var actionSheetButtons: [Alert.Button] {
        var actionButtons = viewModel.filteredAccounts.map { account in
            Alert.Button.default(Text("\(account.name) (\(account.accountType))")){
                if isFromAccount {
                    viewModel.fromAccount = account
                }else{
                    viewModel.toAccount = account
                }
            }
        }
        actionButtons.append(.cancel())
        return actionButtons
    }
    
    var body: some View {
        ZStack {
            
            VStack {
                AccountListView(accounts: viewModel.accounts)
                    .frame(height:300)
                
                TransferBalanceSelectionView(viewModel: viewModel, showSheet: $showSheet, isFromAccount: $isFromAccount)
                
                Text(viewModel.message ?? "")
                
                Button("Submit Transfer"){
                    viewModel.submitTransfer{
                        presentationMode.wrappedValue.dismiss()
                    }
                }.padding()
                    
                    .onAppear{
                        viewModel.populateAccounts()
                    }
                    .actionSheet(isPresented: $showSheet, content: {
                        ActionSheet(
                            title: Text("Transfer Balance"),
                            message: Text("Choose an account"),
                            buttons: actionSheetButtons
                        )
                    })
            }
            .navigationBarTitle("Transfer Balance")
            .embedInNavigationView()
        }
    }
}

struct TransferBalance_Previews: PreviewProvider {
    static var previews: some View {
        TransferBalance()
    }
}
