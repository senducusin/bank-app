//
//  AccountListView.swift
//  Bank App
//
//  Created by Jansen Ducusin on 5/18/21.
//

import SwiftUI

struct AccountListView: View {
    
    let accounts: [AccountViewModel]
    
    var body: some View {
        List(accounts, id: \.accountId){ account in
            AccountListCell(account: account)
        }
    }
}

struct AccountListCell: View {
    
    let account: AccountViewModel
    
    var body: some View {
        HStack{
            VStack(alignment: .leading, spacing: 10){
                Text(account.name)
                    .font(.headline)
                
                Text(account.accountType)
                    .opacity(0.5)
            }
            
            Spacer()
            
            Text("\(account.balance.formatAsCurrency())")
                .foregroundColor(.green)
        }
    }
}
