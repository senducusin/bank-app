//
//  TransferBalanceSelectionView.swift
//  Bank App
//
//  Created by Jansen Ducusin on 5/19/21.
//

import SwiftUI

struct TransferBalanceSelectionView: View {
    
    @ObservedObject var viewModel: TransferBalanceViewModel
    @Binding var showSheet: Bool
    @Binding var isFromAccount: Bool
    
    var body: some View {
        VStack {
            Button("From \(viewModel.fromAccountType)"){
                showSheet = true
                isFromAccount = true
            }
            .frame(maxWidth: .infinity)
            .frame(height:50)
            .background(Color.green)
            .foregroundColor(.white)
            .padding()
            
            Button("To \(viewModel.toAccountType)"){
                showSheet = true
                isFromAccount = false
            }
            .frame(maxWidth: .infinity)
            .frame(height:50)
            .background(Color.green)
            .foregroundColor(.white)
            .padding()
            .opacity(viewModel.fromAccount != nil ? 1 : 0.5)
            .disabled(viewModel.fromAccount == nil)
            
            TextField("Amount",text: $viewModel.amount)
                .padding()
                .textFieldStyle(RoundedBorderTextFieldStyle())
            
            Spacer()
        }
    }
}
