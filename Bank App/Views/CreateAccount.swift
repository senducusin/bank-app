//
//  CreateAccount.swift
//  Bank App
//
//  Created by Jansen Ducusin on 5/18/21.
//

import SwiftUI

struct CreateAccount: View {
    
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject private var viewModel = CreateAccountViewModel()
    
    var body: some View {
        Form {
            TextField("Name", text: $viewModel.name)
            
            Picker(selection: $viewModel.accountType,
                   label: EmptyView()){
                ForEach(AccountType.allCases, id: \.self){ accountType in
                    Text(accountType.title).tag(accountType)
                    
                }
            }.pickerStyle(SegmentedPickerStyle())
         
            TextField("Balance",text:$viewModel.balance)
            
            HStack {
                Spacer()
                Button("Submit"){
                    viewModel.createAccount { result in
                        if result {
                            presentationMode.wrappedValue.dismiss()
                        }
                    }
                }
                Spacer()
            }
            
            Text(viewModel.errorMessage)
        }
        .navigationBarTitle("Add Account")
        .embedInNavigationView()
    }
}

struct CreateAccount_Previews: PreviewProvider {
    static var previews: some View {
        CreateAccount()
    }
}
