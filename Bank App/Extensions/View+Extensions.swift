//
//  View+Extensions.swift
//  Bank App
//
//  Created by Jansen Ducusin on 5/17/21.
//

import SwiftUI

extension View {
    func embedInNavigationView() -> some View {
        NavigationView {self}
    }
}
