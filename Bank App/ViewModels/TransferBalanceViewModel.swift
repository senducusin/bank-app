//
//  TransferBalanceViewModel.swift
//  Bank App
//
//  Created by Jansen Ducusin on 5/18/21.
//

import Foundation

class TransferBalanceViewModel: ObservableObject {
    var fromAccount: AccountViewModel?
    var toAccount: AccountViewModel?
    
    @Published var message: String?
    @Published var accounts = [AccountViewModel]()
    
    var amount: String = ""
    
    var isAmountValid: Bool {
        guard let userBalance = Double(amount) else {
            return false
        }
        
        return userBalance <= 0 ? false : true
    }
    
    var filteredAccounts: [AccountViewModel]{
        if self.fromAccount == nil {
            return accounts
        }else{
            return accounts.filter{
                
                guard let fromAccount = self.fromAccount else {
                    return false
                }
                
                return $0.accountId != fromAccount.accountId
                
            }
        }
    }
    
    var fromAccountType: String {
        fromAccount != nil ? fromAccount!.accountType : ""
    }
    
    var toAccountType: String {
        toAccount != nil ? toAccount!.accountType : ""
    }
    
    private func isValid() -> Bool {
        return isAmountValid
    }
    
    func submitTransfer(completion:@escaping(()->())){
        message = ""
        guard isValid(),
              let fromAccount = fromAccount,
              let toAccount = toAccount,
              let amount = Double(amount) else {
            return
        }
        
        let transferRequest = TransferAccountBalance(
            accountFromId: fromAccount.accountId.lowercased(),
            accountToId: toAccount.accountId.lowercased(),
            amount: amount)
        
        guard let resource = TransferAccountBalance.transferBalance(transferRequest: transferRequest) else {return}
        
        WebService.shared.load(resource: resource) { [weak self] result in
            switch result {
            
            case .success(let response):
                if response.status {
                completion()
                }else{
                    self?.message = response.message
                }
            case .failure(let error):
                self?.message = error.localizedDescription
                print(error.localizedDescription)
            }
        }
    }
    
    func populateAccounts(){
        
        guard let resource = Account.all() else {return}
        
        WebService.shared.load(resource: resource) { [weak self] result in
            switch result {
            
            case .success(let accounts):
                DispatchQueue.main.async {
                    self?.accounts = accounts.map((AccountViewModel.init))
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    
}
