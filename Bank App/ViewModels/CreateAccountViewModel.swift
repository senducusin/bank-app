//
//  CreateAccountViewModel.swift
//  Bank App
//
//  Created by Jansen Ducusin on 5/18/21.
//

import Foundation

class CreateAccountViewModel: ObservableObject {
    var name: String = ""
    var accountType: AccountType = .checking
    var balance: String = ""
    
    @Published var errorMessage: String = ""
    
}

extension CreateAccountViewModel {
    private var isNameValid: Bool {
        !name.isEmpty
    }
    
    private var isBalanceValid: Bool {
        guard let balance = Double(balance) else {return false}
        
        return balance <= 0 ? false : true
    }
    
    private func isValid() -> Bool {
        
        
        var errors = [String]()
        
        if !isNameValid {
            errors.append("Name is not valid")
        }
        
        if !isBalanceValid {
            errors.append("Balance is not valid")
        }
        
        print("DEBUG: errors \(errors)")
        
        if !errors.isEmpty {
            self.errorMessage = errors.joined(separator: "\n")
            return false
        }
        
        return true
    }
}

extension CreateAccountViewModel {
    func createAccount(completion:@escaping((Bool)->())){
        print("DEBUG: called?")
        
        guard isValid(), let balance = Double(balance) else {
            completion(false)
            return
        }
        
        let newAccount = Account(name: name, accountType: accountType, balance: balance)
        
        guard let resource = Account.create(account: newAccount) else {return}
        WebService.shared.load(resource: resource) { [weak self] result in
            switch result {
            
            case .success(let response):
                if let error = response.message {
                    DispatchQueue.main.async {
                        self?.errorMessage = error
                    }
                }
                completion(response.status)
            case .failure(let error):
                print(error.localizedDescription)
                completion(false)
            }
        }
    }
}
