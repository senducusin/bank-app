//
//  AccountSummaryViewModel.swift
//  Bank App
//
//  Created by Jansen Ducusin on 5/18/21.
//

import Foundation

class AccountSummaryViewModel: ObservableObject {
    
    private var _accountModels = [Account]()
    
    @Published var accounts = [AccountViewModel]()
    
    var total: Double {
        _accountModels.map{$0.balance}.reduce(0, +)
    }
    
    func getAllAccounts() {
        
        guard let resource = Account.all() else {return}
        
        WebService.shared.load(resource: resource) { [weak self] result in
            switch result {
            
            case .success(let accounts):
                self?._accountModels = accounts
                
                DispatchQueue.main.async {
                    self?.accounts = accounts.map(AccountViewModel.init)
                }
                
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func createAccount(_ account: Account){
        
        guard let resource = Account.create(account: account) else {return}
        
        WebService.shared.load(resource: resource) { [weak self] result in
            switch result {
            
            case .success(let response):
                DispatchQueue.main.async {
                    if response.status {
                        self?.getAllAccounts()
                    }else{
                        if let message = response.message {
                            print(message)
                        }
                    }
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
